/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ryanspc
 */
public class InventoryTest {
    
    public InventoryTest() {
    }
    
    /**
     * Test of addProduct and Remove method, of class Inventory.
     * Add a product and take the product out of the Inventory
     */
    @Test
    public void testAddProductAndRemoveProduct() {
         System.out.println("Brings in product and Takes it out");
        int skuNumber = 1;
        Product product = new Product(Product.Category.ELECTRONICS,1,2.00,"Milk",500, "White");;
        Inventory instance = new Inventory();
        instance.addProduct(product);
        System.out.println("added");
        boolean expResult = true;
        boolean result = instance.removeProduct(skuNumber);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addProduct and Remove method, of class Inventory.
     * Add a product and take the product out  another product that is not in the Inventory
     */
    @Test
    public void testAddProductAndRemoveProduct2() {
        System.out.println("Brings in product and Takes out one thats not in the inventory");
        int skuNumber = 5;
        Product product = new Product(Product.Category.ELECTRONICS,1,2.00,"Milk",500, "White");;
        Inventory instance = new Inventory();
        instance.addProduct(product);
        System.out.println("added");
        boolean expResult = false;
        boolean result = instance.removeProduct(skuNumber);
        assertEquals(expResult, result);
    }
    
      /**
     * Test of addProduct and Remove method, of class Inventory.
     * Add products and take the product out another product that is not among 
     * a list of multiple products
     */
    @Test
    public void testAddProductAndRemoveProduct3() {
        System.out.println("Brings in product and Takes out one thats not in the inventory");
        int skuNumber = 5;
        Product product = new Product(Product.Category.ELECTRONICS,1,2.00,"Milk",500, "White");
        Product product2 = new Product(Product.Category.ELECTRONICS,15,15.00,"Wine",500, "White");
        Product product3 = new Product(Product.Category.ELECTRONICS,25,0.50,"Candy",500, "White");
        Inventory instance = new Inventory();
        instance.addProduct(product);
        instance.addProduct(product2);
        instance.addProduct(product3);
        System.out.println("added");
        boolean expResult = false;
        boolean result = instance.removeProduct(skuNumber);
        assertEquals(expResult, result);
    }
    
       /**
     * Test of addProduct and Remove method, of class Inventory.
     * Add product and take the product out another product that is among 
     * a list of multiple products
     */
    @Test
    public void testAddProductAndRemoveProduct4() {
        System.out.println("Brings in product and Takes out one thats not in the inventory");
        int skuNumber = 25;
        Product product = new Product(Product.Category.ELECTRONICS,1,2.00,"Milk",500, "White");
        Product product2 = new Product(Product.Category.ELECTRONICS,15,15.00,"Wine",500, "White");
        Product product3 = new Product(Product.Category.ELECTRONICS,25,0.50,"Candy",500, "White");
        Inventory instance = new Inventory();
        instance.addProduct(product);
        instance.addProduct(product2);
        instance.addProduct(product3);
        System.out.println("added");
        boolean expResult = true;
        boolean result = instance.removeProduct(skuNumber);
        assertEquals(expResult, result);
    }
}
