import javax.swing.*;
import javax.swing.event.*;
import java.text.DecimalFormat;
import java.awt.*;
import java.util.ArrayList;

/**
 * Serves as a container for the Buyer's homepage. Intended to be instantiated and then
 * placed in the main JFrame in the View, then hidden and shown as needed.
 * 
 * @author Christian Aghyarian
 * @version 28.04.16
 */
public class BuyerHomePanel extends JPanel
{
    /**
     * Constructs the panel
     */
    public BuyerHomePanel(AuctionController controller)
    {        
        try{
            _productCategoryList = new JList (ProductUtility.getAllCategories().toArray(new String[ProductUtility.getAllCategories().size()]));
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        _filteredProductList      = new JList     ();
        _shoppingCartSummaryLabel = new JLabel    ("Shopping Cart Summary"     );
        _subtotalLabel            = new JLabel    ("Subtotal:      $0.00"      );
        _shoppingCartSizeLabel    = new JLabel    ("Items in cart: 0"          );
        _checkoutButton           = new JButton   ("Checkout"                  );
        _addToCartButton          = new JButton   ("Add to cart"               );
        _productCategoryPane      = new JScrollPane(_productCategoryList       );
        _filteredProductPane      = new JScrollPane(_filteredProductList       );   

        this.setLayout(null);
        
        _filteredProductList.setName("buyer_product_list" );
        _productCategoryList.setName("buyer_category_list");

        this.add(_productCategoryPane     );
        this.add(_filteredProductPane     );
        this.add(_shoppingCartSummaryLabel);
        this.add(_subtotalLabel           );
        this.add(_shoppingCartSizeLabel   );
        this.add(_checkoutButton          );
        this.add(_addToCartButton         );

        _productCategoryList.setSelectionMode    (ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        _productCategoryList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        _filteredProductList.setSelectionMode    (ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        _filteredProductList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        
        _productCategoryList.addListSelectionListener(controller);
        _filteredProductList.addListSelectionListener(controller);

        AbsolutePositionUtility.put(this, _productCategoryPane,      300, 50 , 150, 250);
        AbsolutePositionUtility.put(this, _filteredProductPane,      450, 50 , 325, 250);
        AbsolutePositionUtility.put(this, _checkoutButton,           100, 200, 100, 40 );
        AbsolutePositionUtility.put(this, _addToCartButton,          520, 325, 150, 40 );
        AbsolutePositionUtility.put(this, _shoppingCartSummaryLabel, 60 ,  50);
        AbsolutePositionUtility.put(this, _subtotalLabel,            100, 100, 200, 30);
        AbsolutePositionUtility.put(this, _shoppingCartSizeLabel,    100, 130, 200, 30);

        PanelUtility.addEvent(controller, _checkoutButton,    "checkout"    );
        PanelUtility.addEvent(controller, _addToCartButton,   "add_to_cart" );

    }

    public void updateSubtotalLabel(double subtotal)
    {
        DecimalFormat df = new DecimalFormat("0.00");
        String formattedSubtotal = df.format(subtotal);
        _subtotalLabel.setText("Subtotal:      $" + formattedSubtotal);
    }

    public void updateShoppingCartSize(int size)
    {
        _shoppingCartSizeLabel.setText("Items in cart: " + size);
    }

    public void setProductList(ArrayList<String> productList)
    {
        try{
            _filteredProductList.setListData(productList.toArray(new String[productList.size()]));
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public String getSelectedProduct()
    {
        return (String)_filteredProductList.getSelectedValue();
    }

    private JList       _productCategoryList;
    private JList       _filteredProductList;
    private JScrollPane _productCategoryPane;
    private JScrollPane _filteredProductPane;
    private JLabel      _shoppingCartSummaryLabel;
    private JLabel      _subtotalLabel;
    private JLabel      _shoppingCartSizeLabel;
    private JButton     _checkoutButton;
    private JButton     _addToCartButton;

}
