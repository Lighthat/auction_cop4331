import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Locale;
import java.util.*;

/**
 *
 * @author ryanspc
 */
public class Seller extends AbstractUser{
    public static final String TABLE_USERS= "Users";
    public static final String COLUMN_ID = "ID";
    public static final String TABLE_PRODUCTS = "Products";
    public static final String COLUMN_SKU = "SKU";

    //private static String PATH_TO_DB = "/home/caghyarian/Documents/dev/JAVA/Auction";
    private static String PATH_TO_DB = "D:\\dev\\auction\\projPassworddatabase.sqlite";

    private int          _id;
    private String       _username;
    private String       _email;
    private Session      _session;
    private Inventory _inventory;

    public Seller(int id, String username, String email){  
        _id           = id;
        _username     = username;
        _email        = email;
        _session      = new Session();
        _inventory    = populateInventory(id);
    }

    //get methods
    public int          getID           () {return _id;           }

    public String       getUsername     () {return _username;     }

    public Session      getSession      () {return _session;      }

    public String       getEmail        () {return _email;        }

    public Inventory getInventory () {return _inventory; }

    @Override
    public int getType() { return 1; }
    
    public double getRevenue()
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement statement = connection.createStatement();
            ResultSet resultSet = null;
            try{

                //Pull the inventory from the database
                String query = ("SELECT * FROM Users WHERE ID = " + _id);
                resultSet = statement.executeQuery(query);
                if(resultSet.next())
                {
                    return resultSet.getDouble("revenue");
                }            
                return 0.0;
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(resultSet != null)
                {
                    resultSet.close();
                }
                statement.close();
                connection.close();
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return 0.0;
    }
    
    public static void addRevenue(int id, double amount)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement statement = connection.createStatement();
            ResultSet resultSet = null;
            try{

                //Pull the inventory from the database
                String query = ("SELECT * FROM Users WHERE ID = " + id);
                resultSet = statement.executeQuery(query);
                if(resultSet.next())
                {
                    double newRevenue = resultSet.getDouble("revenue") + amount;
                    query = ("UPDATE Users SET revenue="+newRevenue+" WHERE ID = " + id);
                    statement.executeUpdate(query);
                }           
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(resultSet != null)
                {
                    resultSet.close();
                }
                statement.close();
                connection.close();
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    private Inventory populateInventory(int id)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement statement = connection.createStatement();
            ResultSet resultSet = null;
            try{

                //Pull the inventory from the database
                String query = ("SELECT * FROM " + TABLE_USERS + " WHERE " + COLUMN_ID + " = " + id);
                System.out.println(query);
                resultSet = statement.executeQuery(query);
                if(resultSet.next())
                {
                    ArrayList<Product> productList   = new ArrayList<>();
                    String             productIDList = resultSet.getString("List");
                    if(productIDList != null){
                        List<String>       parsedIDList  = Arrays.asList(productIDList.split(","));
                        for(int i = 0; i < parsedIDList.size(); ++i)
                        {
                            query = "SELECT * FROM "+TABLE_PRODUCTS+" WHERE "+COLUMN_SKU+" = "+Integer.parseInt(parsedIDList.get(i));
                            System.out.println(query);
                            resultSet = statement.executeQuery(query);
                            if(resultSet.next())
                            {
                                Product.Category category    = ProductUtility.parseCategory(resultSet.getString("Category"));
                                int              skuNumber   = resultSet.getInt   ("SKU");
                                double           price       = resultSet.getDouble("Price");
                                String           name        = resultSet.getString("name");
                                int              stock       = resultSet.getInt   ("stock");
                                String           description = resultSet.getString("descr");
                                productList.add(new Product(category, skuNumber, price, name, stock, description));
                            }
                        }
                        return new Inventory(productList);
                    }
                    return new Inventory();

                }            
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(resultSet != null)
                {
                    resultSet.close();
                }
                statement.close();
                connection.close();
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

        return null;
    }
}