
/**
 * A personalized Iterator interface, since the default one from Java
 * forces a remove method that we don't need.
 * 
 * @author Christian Aghyarian
 * @version 19.04.16
 */
public interface MyIterator<E>
{
    public boolean hasNext();
    
    public E       next();
}
