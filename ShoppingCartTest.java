/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ryanspc
 */
public class ShoppingCartTest {
    
    public ShoppingCartTest() {
    }
    
    /**
     * Test of add and remove method, of class ShoppingCart.
     * Test if someone is there
     */
    @Test
    public void testAddAndRemoveProduct() {
        System.out.println("Add and Remove Product that is there");
        int skuNumber = 1;
        Product product = new Product(Product.Category.ELECTRONICS,1,2.00,"Milk",500, "White");;
        ShoppingCart instance = new ShoppingCart();
        instance.addProduct(product);
        System.out.println("added");
        boolean expResult = true;
        boolean result = instance.removeProduct(skuNumber);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     /**
     * Test of add and remove method, of class ShoppingCart.
     * Test if someone is there
     */
    @Test
    public void testAddAndRemoveProduct2() {
        System.out.println("Add and Remove Product that is not there");
        int skuNumber = 5;
        Product product = new Product(Product.Category.ELECTRONICS,1,2.00,"Milk",500, "White");;
        ShoppingCart instance = new ShoppingCart();
        instance.addProduct(product);
        System.out.println("added");
        boolean expResult = false;
        boolean result = instance.removeProduct(skuNumber);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    /**
     * Test of Total method, of class ShoppingCart.
     * For one product
     */
    @Test
    public void testTotal() {
        System.out.println("Add of total of one product");
        Product product = new Product(Product.Category.ELECTRONICS,1,2.00,"Milk",500, "White");
        ShoppingCart instance = new ShoppingCart();
        instance.addProduct(product);
        double expResult = 2.00;
        double result = instance.total();
        assertEquals(expResult, result, 2.00);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of Total method, of class ShoppingCart.
     * For More than one
     */
    @Test
    public void testTotal2() {
        System.out.println("Add total of more than one product");
        Product product1 = new Product(Product.Category.ELECTRONICS,1,2.00,"Milk",500, "White");
        Product product2 = new Product(Product.Category.ELECTRONICS,1,15.00,"Wine",500, "White");
        Product product3 = new Product(Product.Category.ELECTRONICS,1,0.50,"Candy",500, "White");
        ShoppingCart instance = new ShoppingCart();
        instance.addProduct(product1);
        instance.addProduct(product2);
        instance.addProduct(product3);
        double expResult = 17.50;
        double result = instance.total();
        System.out.println(result);
        assertEquals(expResult, result,17.50);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
}
