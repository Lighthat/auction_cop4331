import java.util.Observable;
import java.util.Iterator;

/**
 * The Model for the GUI
 * 
 * @author Christian Aghyarian
 * @version 23.04.16
 */
public class AuctionModel extends Observable
{
    public AuctionModel()
    {
        System.out.println("Create Model");
    }

    public AbstractUser validateUser(String username, String password)
    {   
        int loginFlag = AbstractUser.login(username, password);
        if(loginFlag == -1)
        {
            System.out.println("Invalid username/password.");
            return null;
        }
        else if(loginFlag == -2)
        {
            System.out.println("Username already logged in.");
            return null;
        }
        else
        {
            AbstractUser user = AbstractUserFactory.createUser(loginFlag);
            if(user.getType() == 0)
                _windowToShow = "buyer_home";
            else
                _windowToShow = "seller_home";
                
                
                
            setChanged();
            notifyObservers(_windowToShow);
            return user;
        }
    }

    public void showRegisterWindow()
    {
        _windowToShow = "register";
        setChanged();
        notifyObservers(_windowToShow);
    }

    public void showSellerHomeWindow()
    {
        _windowToShow = "seller_home";
        setChanged();
        notifyObservers(_windowToShow);
    }

    public void showBuyerHomeWindow()
    {
        _windowToShow = "buyer_home";
        setChanged();
        notifyObservers(_windowToShow);
    }
    
    public void showBuyerViewProductWindow(Product product, ViewProductPanel panel)
    {
        panel.displayProduct(product);
        panel.setUserType(0);
        _windowToShow = "buyer_view";
        setChanged();
        notifyObservers(_windowToShow);
    }
    
    public void showSellerViewProductWindow(Product product, ViewProductPanel panel)
    {
        panel.displayProduct(product);
        panel.setUserType(1);
        _windowToShow = "seller_view";
        setChanged();
        notifyObservers(_windowToShow);
    }
    
    public void showAddProductWindow()
    {
        _windowToShow = "add_product";
        setChanged();
        notifyObservers(_windowToShow);
    }
    
    public void addToCart(AuctionView view, Buyer user)
    {
        user.getShoppingCart().addProduct(view.getSelectedProduct());
        ProductUtility.updateShoppingCart(view.getSelectedProduct(), user);
        view.updateShoppingCartSize(user.getShoppingCart().size());
        view.updateSubtotal(user.getShoppingCart().total());
    }

    public void registerUser(String username, String password, String email, int type)
    {
        boolean registerFlag = AbstractUser.register(username, password, email, type);
        if(registerFlag == true)
        {
            System.out.println("show login");
            _windowToShow = "login";
            setChanged();
            notifyObservers(_windowToShow);
        }
        else
        {
            System.out.println("show register");
            _windowToShow = "register";
            setChanged();
            notifyObservers(_windowToShow);
        }
    }
    
    public void addProduct(Product product, Seller owner)
    {
        ProductUtility.insertProductIntoDatabase(product, owner);
        _windowToShow = "seller_home";
        setChanged();
        notifyObservers(_windowToShow);
    }
    
    public void setProductListDisplay(String category, AuctionView view)
    {
        view.setProductDisplayCategory(ProductUtility.getProductsOfCategory(category));
    }
    
    public void checkout(Buyer buyer)
    {
        ShoppingCart cart = buyer.getShoppingCart();
        double total = cart.total();
        Iterator cartIterator = cart.iterator();
        while(cartIterator.hasNext())
        {
            ProductUtility.updateStock((Product)cartIterator.next());
        }
        buyer.setShoppingCart(new ShoppingCart());
        ProductUtility.resetShoppingCart(buyer);
    }

    private int     _currentUserID;
    private String _windowToShow;
}