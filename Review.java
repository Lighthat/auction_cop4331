
/**
 * The Review class is a container for the contents of the body of a Review.
 * It's essentially like a struct in C. Products will use this class to keep
 * track of the individual product reviews that users will be able to leave.
 * 
 * @author Christian Aghyarian
 * @version 19.04.16
 */
public class Review
{

    /**
     * Constructor for Review objects
     * @param reviewBody the text of the review
     * @param rating     the rating of the review, ranging from 1-5
     */
    public Review(String reviewBody, int rating)
    {
        _reviewBody = reviewBody;
        if(rating > 5)
            _rating = 5;
        else if (rating < 1)
            _rating = 1;
        else
            _rating = rating;
    }
    
    /** @return the review text of this Review */
    public String getReviewBody() { return _reviewBody; }
    
    /** @return the star rating of this Review */
    public int    getRating    () { return _rating; }
    
    private String _reviewBody;
    private int _rating; //Ranging from 1-5 stars
    
}
