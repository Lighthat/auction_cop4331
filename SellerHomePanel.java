import javax.swing.*;
import javax.swing.event.*;
import java.text.DecimalFormat;
import java.awt.*;
import java.util.ArrayList;

/**
 * Write a description of class SellerHomePanel here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SellerHomePanel extends JPanel
{
    // instance variables - replace the example below with your own
    private int x;

    /**
     * Constructor for objects of class SellerHomePanel
     */
    public SellerHomePanel(AuctionController controller)
    {
        try{
            _productCategoryList = new JList (ProductUtility.getAllCategories().toArray(new String[ProductUtility.getAllCategories().size()]));
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        _filteredProductList      = new JList      ();
        _sellerSummaryLabel       = new JLabel     ("Seller Summary"            );
        _revenueLabel             = new JLabel     ("Revenue:         $0.00"    );
        _inventorySizeLabel       = new JLabel     ("Inventory Size: 0"         );
        _addProductButton         = new JButton    ("Add Product"               );
        _productCategoryPane      = new JScrollPane(_productCategoryList        );
        _filteredProductPane      = new JScrollPane(_filteredProductList        );   

        this.setLayout(null);
        
        _filteredProductList.setName("seller_product_list" );
        _productCategoryList.setName("seller_category_list");

        this.add(_productCategoryPane     );
        this.add(_filteredProductPane     );
        this.add(_sellerSummaryLabel      );
        this.add(_revenueLabel            );
        this.add(_inventorySizeLabel      );
        this.add(_addProductButton        );

        _productCategoryList.setSelectionMode    (ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        _productCategoryList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        _filteredProductList.setSelectionMode    (ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        _filteredProductList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        
        _productCategoryList.addListSelectionListener(controller);
        _filteredProductList.addListSelectionListener(controller);

        AbsolutePositionUtility.put(this, _productCategoryPane,      300, 50 , 150, 250);
        AbsolutePositionUtility.put(this, _filteredProductPane,      450, 50 , 325, 250);
        AbsolutePositionUtility.put(this, _addProductButton,         520, 325, 150, 40 );
        AbsolutePositionUtility.put(this, _sellerSummaryLabel, 60 ,  50);
        AbsolutePositionUtility.put(this, _revenueLabel,            100, 100, 200, 30);
        AbsolutePositionUtility.put(this, _inventorySizeLabel,    100, 130, 200, 30);
        
        PanelUtility.addEvent(controller, _addProductButton,  "seller_add_product" );

    }

    public void updateRevenueLabel(double revenue)
    {
        DecimalFormat df = new DecimalFormat("0.00");
        String formattedRevenue = df.format(revenue);
        _revenueLabel.setText("Revenue:         $" + formattedRevenue);
    }

    public void updateInventorySize(int size)
    {
        _inventorySizeLabel.setText("Inventory Size: " + size);
    }
    
    public String getSelectedProduct()
    {
        return (String)_filteredProductList.getSelectedValue();
    }

    public void setProductList(ArrayList<String> productList)
    {
        try{
            _filteredProductList.setListData(productList.toArray(new String[productList.size()]));
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    private JList       _productCategoryList;
    private JList       _filteredProductList;
    private JScrollPane _productCategoryPane;
    private JScrollPane _filteredProductPane;
    private JLabel      _sellerSummaryLabel;
    private JLabel      _revenueLabel;
    private JLabel      _inventorySizeLabel;
    private JButton     _addProductButton;

}
