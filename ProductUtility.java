import java.util.*;
import java.sql.*;

/**
 * Write a description of class ProductUtility here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public final class ProductUtility
{
    public static ArrayList<String> getAllCategories()
    {
        ArrayList<String> categoryList = new ArrayList<>();

        categoryList.add(Product.Category.HOME_GOODS.text()   );
        categoryList.add(Product.Category.ELECTRONICS.text()  );
        categoryList.add(Product.Category.FURNITURE.text()    );
        categoryList.add(Product.Category.BOOKS.text()        );
        categoryList.add(Product.Category.DIGITAL_GOODS.text());

        return categoryList;
    }

    public static Product.Category parseCategory(String input)
    {
        if(input.equals("Home Goods"))
            return Product.Category.HOME_GOODS;
        else if(input.equals("Electronics"))
            return Product.Category.ELECTRONICS;
        else if(input.equals("Furniture"))
            return Product.Category.FURNITURE;
        else if(input.equals("Books"))
            return Product.Category.BOOKS;
        else if(input.equals("Digital Goods"))
            return Product.Category.DIGITAL_GOODS;
        else
            return Product.Category.INVALID;
    }

    public static void insertProductIntoDatabase(Product product, Seller owner)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement st=con.createStatement();
            ResultSet rs = null;
            try{
                String query = ("INSERT INTO Products (category, price, name, stock, descr, owner) VALUES (\""+product.getCategory()+"\", "+product.getPrice()+", \""+product.getName()+"\", "+product.getStock()+", \""+product.getDescription()+"\", "+owner.getID()+")");
                st.executeUpdate(query);
                owner.getInventory().addProduct(product);
                query = ("SELECT * FROM Products WHERE Owner = " + owner.getID());
                rs = st.executeQuery(query);
                String productList = "";
                while(rs.next())
                {
                    productList+=(rs.getString("SKU"));
                    productList+=",";
                }
                productList = productList.substring(0, productList.length()-1);
                query = ("UPDATE Users SET List=\""+productList+"\" WHERE ID = "+owner.getID());
                st.executeUpdate(query);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(rs != null)
                {
                    rs.close();
                }
                st.close();
                con.close();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }

    public static void updateShoppingCart(Product product, Buyer buyer)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement st=con.createStatement();
            ResultSet rs = null;
            try{
                String query = ("SELECT * FROM Users WHERE ID = " + buyer.getID());
                rs = st.executeQuery(query);
                String shoppingCart = rs.getString("list");
                if (shoppingCart == null)
                {
                    shoppingCart = Integer.toString(product.getSkuNumber());
                }
                else
                {
                    shoppingCart += ",";
                    shoppingCart += product.getSkuNumber();
                }
                query = ("UPDATE Users SET List=\""+shoppingCart+"\" WHERE ID = "+buyer.getID());
                st.executeUpdate(query);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(rs != null)
                {
                    rs.close();
                }
                st.close();
                con.close();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public static void resetShoppingCart(Buyer buyer)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement st=con.createStatement();
            ResultSet rs = null;
            try{
                String query = ("UPDATE Users SET List=\"\" WHERE ID = "+buyer.getID());
                st.executeUpdate(query);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(rs != null)
                {
                    rs.close();
                }
                st.close();
                con.close();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getProductsOfCategory(String category)
    {
        ArrayList<String> list = new ArrayList<>();
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement st=con.createStatement();
            ResultSet rs = null;
            try{
                String query = ("SELECT * FROM Products WHERE category = \"" + category + "\"");
                rs=st.executeQuery(query);

                while(rs.next())
                {
                    list.add(rs.getString("name"));
                }
                return list;
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(rs != null)
                {
                    rs.close();
                }
                st.close();
                con.close();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    //Produts should be identified by their SKU, but because of time constraints we just use name
    public static Product getProduct(String name)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement st=con.createStatement();
            ResultSet rs = null;
            try{
                String query = ("SELECT * FROM Products WHERE name = \"" + name + "\"");
                rs=st.executeQuery(query);
                if(rs.next())
                {
                    Product product = new Product(parseCategory(rs.getString("category")), rs.getInt("SKU"), rs.getDouble("price"), rs.getString("name"), rs.getInt("stock"), rs.getString("descr"));
                    return product;
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(rs != null)
                {
                    rs.close();
                }
                st.close();
                con.close();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateStock(Product product)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement st=con.createStatement();
            ResultSet rs = null;
            try{
                String query = ("SELECT * FROM Products WHERE SKU = " + product.getSkuNumber());
                rs=st.executeQuery(query);
                if(rs.next())
                {
                    int stock = rs.getInt("stock") - 1;
                    double revenue = rs.getDouble("price");
                    int owner = rs.getInt("owner");
                    query = ("UPDATE Products SET stock="+stock+" WHERE SKU = " + product.getSkuNumber());
                    st.executeUpdate(query);
                    Seller.addRevenue(owner, revenue);
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(rs != null)
                {
                    rs.close();
                }
                st.close();
                con.close();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private ProductUtility() { }
    //private static String PATH_TO_DB = "/home/caghyarian/Documents/dev/JAVA/Auction";
    private static String PATH_TO_DB = "D:\\dev\\auction\\projPassworddatabase.sqlite";
}
