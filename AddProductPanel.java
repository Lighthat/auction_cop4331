import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Write a description of class AddProductPanel here.
 * 
 * @author Christian Aghyarian
 * @version 01.05.2016
 */
public class AddProductPanel extends JPanel
{
    public AddProductPanel(AuctionController controller)
    {
        ArrayList<String> productCategoryList = new ArrayList<>(ProductUtility.getAllCategories());
        
        _categoryLabel        = new JLabel("Category:");
        _nameLabel            = new JLabel("Name:");
        _priceLabel           = new JLabel("Price:");
        _stockLabel           = new JLabel("Stock:");
        _descriptionLabel     = new JLabel("Description: ");
        _categoryDropdown     = new JComboBox(productCategoryList.toArray());
        _nameTextField        = new JTextField();
        _priceTextField       = new JTextField();
        _stockTextField       = new JTextField();
        _descriptionTextField = new JTextArea();
        _descriptionTextPane  = new JScrollPane(_descriptionTextField);
        _addProductButton     = new JButton("Add Product");
        
        this.setLayout(null);
        
        this.add(_categoryLabel);
        this.add(_nameLabel);
        this.add(_priceLabel);
        this.add(_stockLabel);
        this.add(_descriptionLabel);
        this.add(_categoryDropdown);
        this.add(_nameTextField);
        this.add(_priceTextField);
        this.add(_stockTextField);
        this.add(_descriptionTextPane);
        this.add(_addProductButton);
        
        AbsolutePositionUtility.put(this, _categoryLabel,            0  , 0  , 150, 30 );
        AbsolutePositionUtility.put(this, _nameLabel,                0  , 50 , 150, 30 );
        AbsolutePositionUtility.put(this, _priceLabel,               0  , 100, 150, 30 );
        AbsolutePositionUtility.put(this, _stockLabel,               0  , 150, 150, 30 );
        AbsolutePositionUtility.put(this, _descriptionLabel,         0  , 200, 150, 30 );
        AbsolutePositionUtility.put(this, _categoryDropdown,         150, 0  , 250, 30 );
        AbsolutePositionUtility.put(this, _nameTextField,            150, 50 , 250, 30 );
        AbsolutePositionUtility.put(this, _priceTextField,           150, 100, 250, 30 );
        AbsolutePositionUtility.put(this, _stockTextField,           150, 150, 250, 30 );
        AbsolutePositionUtility.put(this, _descriptionTextPane,      150, 200, 300, 200);
        AbsolutePositionUtility.put(this, _addProductButton,         150, 450, 150, 50 );
        
        PanelUtility.addEvent(controller, _addProductButton, "add_product_final");
    }
    
    public String getDropdownValue() { return (String)_categoryDropdown.getSelectedItem(); }
    
    public String getNameFieldContents() { return _nameTextField.getText(); }
    
    public String getPriceFieldContents() { return _priceTextField.getText(); }
    
    public String getStockFieldContents() { return _stockTextField.getText(); }
    
    public String getDescriptionFieldContents() { return _descriptionTextField.getText(); }
    
    private JLabel _categoryLabel;
    private JLabel _nameLabel;
    private JLabel _priceLabel;
    private JLabel _stockLabel;
    private JLabel _descriptionLabel;
    private JComboBox _categoryDropdown;
    private JTextField _nameTextField;
    private JTextField _priceTextField;
    private JTextField _stockTextField;
    private JTextArea  _descriptionTextField;
    private JScrollPane _descriptionTextPane;
    private JButton _addProductButton;
}
