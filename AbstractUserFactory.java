import java.sql.*;

/**
 * A Factory class to allow the Model to create a general User without knowing what type they are.
 * 
 * @author Christian Aghyarian 
 * @version 29.04.16
 */
public class AbstractUserFactory
{    
    public static final String COLUMN_Username = "UserName";
    public static final String COLUMN_Type     = "Type";
    public static final String COLUMN_ID       = "ID";
    public static final String COLUMN_PW       = "Password";
    public static final String TABLE_USERS     = "Users";

    //private static String PATH_TO_DB = "/home/caghyarian/Documents/dev/JAVA/Auction";
    private static String PATH_TO_DB = "D:\\dev\\auction\\projPassworddatabase.sqlite";

    public static AbstractUser createUser(int id)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement statement = connection.createStatement();
            ResultSet resultSet = null;
            try
            {
                //Get the user type from the DB
                String query = ("SELECT * FROM " + TABLE_USERS + " WHERE " + COLUMN_ID + " = " + id + ";");
                System.out.println(query);
                resultSet = statement.executeQuery(query);
                if(resultSet.next())
                {
                    if(resultSet.getInt("Type") == 0)
                    {
                        String username = resultSet.getString("UserName");
                        String email    = resultSet.getString("email");
                        return new Buyer(id, username, email);
                    }
                    else
                    {
                        System.out.println("make seller");
                        String username = resultSet.getString("UserName");
                        String email    = resultSet.getString("email");
                        return new Seller(id, username, email);
                    }
                }
                return null;
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
                return null;
            }
            finally
            {
                if(resultSet != null)
                {
                    resultSet.close();
                }
                statement.close();
                connection.close();
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return null;

    }
}
