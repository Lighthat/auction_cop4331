import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Observer;
import java.util.Observable;
import java.util.ArrayList;

/**
 * The View for the GUI
 * 
 * @author Christian Aghyarian
 * @version 27.04.16
 */
public class AuctionView implements Observer
{
    /**
     * The constructor for the View will prompt the user to log in.
     * The corresponding frames will be created and shown based on user input
     * once they've logged in.
     */
    public AuctionView(AuctionController controller)
    {
        _controller           = controller;

        _model                = new AuctionModel    (                  );
        JFrame frame          = new JFrame          ("Auctionator"     );
        _loginPanel           = new LoginPanel      (_controller);
        _buyerHomePanel       = new BuyerHomePanel  (_controller);
        _registerPanel        = new RegisterPanel   (_controller);
        _sellerHomePanel      = new SellerHomePanel (_controller);
        _viewProductPanel     = new ViewProductPanel(_controller);
        _addProductPanel      = new AddProductPanel (_controller);

        _model.addObserver(this);
        frame.setLayout(null);
        frame.add(_registerPanel);
        frame.add(_loginPanel);
        frame.add(_buyerHomePanel);
        frame.add(_sellerHomePanel);
        frame.add(_viewProductPanel);
        frame.add(_addProductPanel);

        AbsolutePositionUtility.put(frame, _registerPanel,    0  , 0  , 800, 600);
        AbsolutePositionUtility.put(frame, _loginPanel,       225, 200, 800, 600);
        AbsolutePositionUtility.put(frame, _buyerHomePanel,   0  , 50 , 800, 600);
        AbsolutePositionUtility.put(frame, _sellerHomePanel,  0  , 50 , 800, 600);
        AbsolutePositionUtility.put(frame, _viewProductPanel, 0  , 0  , 800, 600);
        AbsolutePositionUtility.put(frame, _addProductPanel,  200, 50 , 800, 600);

        _registerPanel.setVisible(false);
        _buyerHomePanel.setVisible(false);
        _sellerHomePanel.setVisible(false);
        _loginPanel.setVisible(true);
        _viewProductPanel.setVisible(false);
        _addProductPanel.setVisible(false);

        _viewProductPanel.setUserType(0);

        frame.addWindowListener(new CloseListener());
        frame.setSize          (800,600);
        frame.setLocation      (100,100);
        frame.setVisible       (true);

    }

    @Override
    public void update(Observable obs, Object obj)
    {
        System.out.println ("View: Observable is " + obs.getClass() + ", object passed is " + obj.getClass());
        showWindow((String)obj);

    }

    public AuctionModel getModel() { return _model; }

    public String getLoginUsernameFieldContents() { return _loginPanel.getUsernameFieldContents(); }

    public String getLoginPasswordFieldContents() { return _loginPanel.getPasswordFieldContents(); }

    public String getRegisterUsernameFieldContents() { return _registerPanel.getUsernameFieldContents(); }

    public String getRegisterPasswordFieldContents() { return _registerPanel.getPasswordFieldContents(); }

    public String getRegisterEmailFieldContents() { return _registerPanel.getEmailFieldContents(); }

    public String getAddProductCategoryContents() { return _addProductPanel.getDropdownValue(); }

    public String getAddProductNameFieldContents() { return _addProductPanel.getNameFieldContents(); }

    public String getAddProductPriceFieldContents() { return _addProductPanel.getPriceFieldContents(); }

    public String getAddProductStockFieldContents() { return _addProductPanel.getStockFieldContents(); }

    public String getAddProductDescriptionFieldContents() { return _addProductPanel.getDescriptionFieldContents(); }

    public AbstractUser getActiveUser() { return _activeUser; }

    public void setActiveUser(AbstractUser user) 
    { 
        _activeUser = user;
        if(user.getType() == 0)
        {
            Buyer buyer = (Buyer)_activeUser;
            _buyerHomePanel.updateShoppingCartSize(buyer.getShoppingCart().size());
            _buyerHomePanel.updateSubtotalLabel(buyer.getShoppingCart().total());
        }
        else
        {
            Seller seller = (Seller)_activeUser;
            _sellerHomePanel.updateInventorySize(seller.getInventory().size());
            _sellerHomePanel.updateRevenueLabel(seller.getRevenue());
        }
    }

    public ViewProductPanel getViewProductPanel() { return _viewProductPanel; }

    public void updateShoppingCartSize(int size) { _buyerHomePanel.updateShoppingCartSize(size); }
    
    public void updateSubtotal(double subtotal) { _buyerHomePanel.updateSubtotalLabel(subtotal); }
    
    public void updateRevenue(double revenue) { _sellerHomePanel.updateRevenueLabel(revenue); }

    public void updateInventorySize(int size) { _sellerHomePanel.updateInventorySize(size); }

    public Product getSelectedProduct()
    {
        if(_activeUser.getType() == 0)
            return ProductUtility.getProduct(_buyerHomePanel.getSelectedProduct());
        else
            return ProductUtility.getProduct(_sellerHomePanel.getSelectedProduct());

    }

    public void setProductDisplayCategory(ArrayList<String> list)
    { 
        _sellerHomePanel.setProductList(list);
        _buyerHomePanel.setProductList(list);
    }

    private void showWindow(String window)
    {
        if(window.equals("register"))
        {
            _loginPanel.setVisible(false);
            _buyerHomePanel.setVisible(false);
            _registerPanel.setVisible(true);
            _sellerHomePanel.setVisible(false);
            _viewProductPanel.setVisible(false);
            _addProductPanel.setVisible(false);
        }
        else if(window.equals("login"))
        {
            _loginPanel.setVisible(true);
            _buyerHomePanel.setVisible(false);
            _registerPanel.setVisible(false);
            _sellerHomePanel.setVisible(false);
            _viewProductPanel.setVisible(false);
            _addProductPanel.setVisible(false);
        }
        else if(window.equals("buyer_home"))
        {
            _loginPanel.setVisible(false);
            _buyerHomePanel.setVisible(true);
            _registerPanel.setVisible(false);
            _sellerHomePanel.setVisible(false);
            _viewProductPanel.setVisible(false);
            _addProductPanel.setVisible(false);
        }
        else if(window.equals("seller_home"))
        {
            _loginPanel.setVisible(false);
            _buyerHomePanel.setVisible(false);
            _registerPanel.setVisible(false);
            _sellerHomePanel.setVisible(true);
            _viewProductPanel.setVisible(false);
            _addProductPanel.setVisible(false);
        }
        else if(window.equals("seller_view"))
        {
            _loginPanel.setVisible(false);
            _buyerHomePanel.setVisible(false);
            _registerPanel.setVisible(false);
            _sellerHomePanel.setVisible(false);
            _viewProductPanel.setVisible(true);
            _viewProductPanel.setUserType(1);
            _addProductPanel.setVisible(false);
        }
        else if(window.equals("buyer_view"))
        {
            _loginPanel.setVisible(false);
            _buyerHomePanel.setVisible(false);
            _registerPanel.setVisible(false);
            _sellerHomePanel.setVisible(false);
            _viewProductPanel.setVisible(true);
            _viewProductPanel.setUserType(0);
            _addProductPanel.setVisible(false);
        }
        else if(window.equals("add_product"))
        {
            _loginPanel.setVisible(false);
            _buyerHomePanel.setVisible(false);
            _registerPanel.setVisible(false);
            _sellerHomePanel.setVisible(false);
            _viewProductPanel.setVisible(false);
            _addProductPanel.setVisible(true);
        }
    }

    public static class CloseListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent e)
        {
            e.getWindow().setVisible(false);
            System.exit(0);
        }
    }

    private AuctionModel      _model;
    private AuctionController _controller;
    private LoginPanel        _loginPanel;
    private BuyerHomePanel    _buyerHomePanel;
    private RegisterPanel     _registerPanel;
    private SellerHomePanel   _sellerHomePanel;
    private ViewProductPanel  _viewProductPanel;
    private AddProductPanel   _addProductPanel;

    private AbstractUser      _activeUser;
}
