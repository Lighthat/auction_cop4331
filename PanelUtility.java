import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

/**
 * This is a static class that contains a series of useful utility methods when using the
 * MVC pattern and have mutliple panels in separate classes. This utility helps when you
 * want to keep everything centralized in your View, but you have information from your
 * Model or your entry point that needs to get to your panels.
 * 
 * Static classes don't formally exist in Java, but this is functionally a static class
 * as it is declared as final, and has a private constructor. It should never be
 * instantiated, as described by the Utility design pattern.
 * 
 * @author Christian Aghyarian
 * @version 27.04.16
 */
public final class PanelUtility
{
    /**
     * Adds a listener to the provided controller for the specified button, with the specified
     * Action Command
     * 
     * @param controller the controller you wish to use
     * @param button the button you wish to tie the listener to
     * @param actionCommand the action command you wish to set for this action
     */
    public static void addEvent(ActionListener controller, JButton button, String actionCommand)
    {
        button.addActionListener(controller);
        button.setActionCommand (actionCommand);
    }
    
    public static void addEvent(ActionListener controller, JComboBox comboBox, String actionCommand)
    {
        comboBox.addActionListener(controller);
        comboBox.setActionCommand (actionCommand);
    }
    
    private PanelUtility() { }
}
