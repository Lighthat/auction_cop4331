/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ryanspc
 */
public class AbstractUserTest {
  /**
     * Test of Login method, of class AbstractUser.
     * Checks to see if it Log ins a person in the database 
     */
    @Test
    public void testLogin() {
        System.out.println("Customer Login");
        String username = "ryan";
        String password = "1234";
        int expResult = 1;
        int result = AbstractUser.login(username, password);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
/**
     * Test of Login method, of class AbstractUser.
     * Checks to see if it Log ins a person in the database if User name is wrong
     */
    @Test
    public void testLogin2() {
        System.out.println("Checks when user name is wrong");
        String username = "Ryan";
        String password = "1234";
        int expResult = -1 ;
        int result = AbstractUser.login(username, password);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
   
    /**
     * Test of register method, of class AbstractUser.
     * Checks to see if it recognizes a new person in the database
     */
    @Test
    public void testRegister() {
        System.out.println("register");
        String username = "bob";
        String password = "something";
        String email = "idontknow@me.com";
        int type = 4;
        boolean expResult = true;
        boolean result = AbstractUser.register(username, password, email, type);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of register method, of class AbstractUser.
     * Checks to see if it recognizes person who is already in the database
     */
    @Test
     public void testRegister2() {
        System.out.println("register if the name is in the system already");
        String username = "ryan";
        String password = "1234";
        String email = "idontknow@me.com";
        int type = 1;
        boolean expResult = false;
        boolean result = AbstractUser.register(username, password, email, type);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

}
