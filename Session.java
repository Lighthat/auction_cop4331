import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.security.*;

/**
 * Serves as a unique handler for making sure a user can only be logged in in one place,
 * while also making sure that they don't stay logged in for too long
 * 
 * @author Christian Aghyarian
 * @version 23.04.16
 */
public class Session
{

    /**
     * This constructor will automatically create a 64-character long session key
     * and assign the expiration date of the Session 24 hours in the future.
     */
    public Session()
    {
        Random rand = new Random();
        //generate the session key
        _sessionKey = generateString(rand, "[]!@#$%^&*()1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 64);
        
        //set the expiration time exactly 24 hrs in the future
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        _expirationTime = calendar.getTime();
    }
    
    public String getSessionKey  () { return _sessionKey;     }
    
    public Date getExpirationTime() { return _expirationTime; }
    
    /**
     * This constructor will automatically create a 64-character long session key
     * and assign the expiration date of the Session some time in the future.
     * 
     * @param expirationTime how many days in the future you wish the session to expire
     */
    public Session(int expirationTime)
    {
        Random rand = new Random();
        //generate the session key
        _sessionKey = generateString(rand, "[]!@#$%^&*()1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 64);
        
        //set the expiration time exactly 24 hrs in the future
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, expirationTime);
        _expirationTime = calendar.getTime();
    }
    
    /**
     * Deletes the reference to this Session in the database,
     * as well as setting the fields to null to mark them
     * for deletion by the garbage collector (assuming the
     * Session object itself is also set to null as it should
     * be)
     */
    public void destroy()
    {
        //Insert an sql statement that will find this session key in the database and set it to null
        //Make sure the sql statements go BEFORE the null statements.
        
        _sessionKey = null;
        _expirationTime = null;
    }
    
    /**
     * Generates a random string.
     * 
     * @param rng The random number generator you wish to use
     * @param characters The character set you wish to pull from when generating the random string
     * @param length How long you want the generated string to be
     * @return A random string with the specified parameters.
     */
    public String generateString(Random rng, String characters, int length)
    {
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }
    
    /** A random hash to ensure each Session is unique */
    private String _sessionKey;
    /** The date when this Session should expire       */
    private Date   _expirationTime;
}
