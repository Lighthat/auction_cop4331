
import java.util.ArrayList;

/**
 * This class serves as a container for the information of any product that
 * a user will be able to interact with on the View. 
 * 
 * @author Christian Aghyarian
 * @version 19.04.16
 */
public class Product
{

    /** This class is best used with this constructor to avoid the obvious issue
     *  of having a constructor with 7 arguments.
     */
    public Product()
    {

    }

    /** Although if you like, you can use this constructor too.
     * @param category    The category this product will be grouped under
     * @param skuNumber   the SKU Number of this product
     * @param price       How much the product will cost in USD
     * @param name        The name of the product
     * @param stock       Available quantity of this product
     * @param description A description of the product
     */
    public Product(Category category, int skuNumber, double price, String name, int stock, String description)
    {
        _category = category;
        _skuNumber = skuNumber;
        _price = price;
        _name = name;
        _stock = stock;
        _description = description;
    }

    //SETTERS
    public void setCategory   (Category category)  { _category    = category;    }

    public void setSkuNumber  (int skuNumber)      { _skuNumber   = skuNumber;   }

    public void setPrice      (double price)       { _price       = price;       }

    public void setName       (String name)        { _name        = name;        }

    public void setStock      (int stock)          { _stock       = stock;       }

    public void setDescription(String description) { _description = description; }

    //GETTERS
    public String            getCategory()    { return _category.text(); }

    public int               getSkuNumber()   { return _skuNumber;       }

    public double            getPrice()       { return _price;           }

    public String            getName()        { return _name;            }

    public int               getStock()       { return _stock;           }

    public String            getDescription() { return _description;     }

    public enum Category
    {
        HOME_GOODS    ("Home Goods"),

        ELECTRONICS   ("Electronics"),

        FURNITURE     ("Furniture"),

        BOOKS         ("Books"),

        DIGITAL_GOODS ("Digital Goods"),

        INVALID       ("Invalid");

        Category(String categoryName)
        {
            __categoryName = categoryName;
        }

        public String text() { return __categoryName; }

        private String __categoryName;
    }

    private Category          _category;
    private int               _skuNumber;
    private double            _price;
    private String            _name;
    private int               _stock;
    private String            _description;
}
