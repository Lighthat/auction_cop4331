
import java.util.*;
/**
 *
 * @author ryanspc
 */
public class ShoppingCart implements Iterable{
    
    //creates a new shopping cart if the owner does not have a shopping cart
    public ShoppingCart()
    {
        _productList = new ArrayList<>();
    }
    
    //this is the shopping cart if the owner already has a list 
    public ShoppingCart( ArrayList<Product> list)
    {
        _productList = new ArrayList<>(list);
    }
    
    //This gets the Iterator
    public Iterator iterator()
    {
         return new SCIterator();
    }
    
    //this add a product to the shopping cart
    public void addProduct(Product product)
    {
        _productList.add(product);
    }
    
    public int size()
    {
        return _productList.size();
    }
    
    //returns a true or false value if you can not find the item in the shopping cart
    public boolean removeProduct(int skuNumber)
    {
        for(Product P:_productList)
        {
            if(P.getSkuNumber() == skuNumber)
            {
                _productList.remove(P);
                return true;
            }
        }return false;
    }
    //this function sums of the total of products in the shopping cart
    public double total()
    {
        double sum = 0;
        
        for(Product P:_productList)
        {
                sum += P.getPrice();
        }       
           return sum;
    }

    //this is the iterator class that is used to cycle through the list in the 
    //shopping cart.
    private class SCIterator implements Iterator
    {
        @Override
        public boolean hasNext()
        {
            if(index < _productList.size())
            {
                return true;
            }
            else{
                return false;
            }
        }
        
        public Product next()
        {
            if(this.hasNext())
                return _productList.get(index++);
            else
                return null;
        }
        
        public void remove()
        {
            
        }
       int index;
    }    
    
    private ArrayList<Product> _productList;
    
    
}