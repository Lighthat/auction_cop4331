/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ryanspc
 */
public class BuyerTest {
    
    public BuyerTest() {
    }

    /**
     * Test of getShoppingCart method, of class Buyer.
     * Test to show a person with list that is already filled with the amount it
     * takes to compile
     */
    @Test
    public void testGetShoppingCart() {
        System.out.println("getShoppingCart");
        Buyer instance = new Buyer(1,"Ryan", "");
        ShoppingCart expResult = null;
        ShoppingCart result = instance.getShoppingCart();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     /**
     * Test of getShoppingCart method, of class Buyer.
     * Test to show a person with nothing in its list 
     */
    @Test
    public void testGetShoppingCart2() {
        System.out.println("getShoppingCart");
        Buyer instance = new Buyer(2,"lauren", "");
        ShoppingCart expResult = null;
        ShoppingCart result = instance.getShoppingCart();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
      /**
     * Test of getShoppingCart method, of class Buyer.
     * A user that does not exist in the database
     */
    @Test
    public void testGetShoppingCart3() {
        System.out.println("getShoppingCart");
        Buyer instance = new Buyer(3,"someoneelse", "");
        ShoppingCart expResult = null;
        ShoppingCart result = instance.getShoppingCart();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
}
