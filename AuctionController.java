import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;

/**
 * the Controller for the GUI
 * 
 * @author Christian Aghyarian
 * @version 23.04.16
 */
public class AuctionController implements ActionListener, ListSelectionListener
{    
    public AuctionController()
    {
        System.out.println("Create Controller");
    }

    public void actionPerformed(ActionEvent e)
    {
        parseActionCommand(e.getActionCommand());
    }

    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {
            JList list = (JList)e.getSource();
            System.out.println(list.getName());
            
            if(list.getName().equals("seller_category_list") || list.getName().equals("buyer_category_list"))
                parseCategoryListSelectionEvent(list.getMaxSelectionIndex());
            else
                parseProductListSelectionEvent(list.getMaxSelectionIndex(), list);
                

        }
    }

    public void addModel(AuctionModel m)
    {
        this.model = m;
    }

    public void addView(AuctionView v)
    {
        this.view = v;
    }

    private void parseActionCommand(String actionCommand)
    {
        if     (actionCommand.equals("login"))
        {
            view.setActiveUser(model.validateUser(
                    view.getLoginUsernameFieldContents(), 
                    view.getLoginPasswordFieldContents()
                )
            );
        }
        else if(actionCommand.equals("exit"))
            System.exit(0);
        else if(actionCommand.equals("show_register_window"))
        {
            model.showRegisterWindow();
        }
        else if(actionCommand.equals("register_buyer"))
        {
            model.registerUser(
                view.getRegisterUsernameFieldContents(),
                view.getRegisterPasswordFieldContents(),
                view.getRegisterEmailFieldContents   (),
                0);
        }
        else if(actionCommand.equals("register_seller"))
        {
            model.registerUser(
                view.getRegisterUsernameFieldContents(),
                view.getRegisterPasswordFieldContents(),
                view.getRegisterEmailFieldContents   (),
                1);
        }
        else if(actionCommand.equals("checkout"))
        {
            model.checkout((Buyer)view.getActiveUser());
            view.updateShoppingCartSize(0);
            view.updateSubtotal(0.00);
        }
        else if(actionCommand.equals("add_to_cart"))
            model.addToCart(view, (Buyer)view.getActiveUser());
        else if(actionCommand.equals("seller_add_product"))
            model.showAddProductWindow();
        else if(actionCommand.equals("seller_return_home"))
            model.showSellerHomeWindow();
        else if(actionCommand.equals("buyer_return_home"))
            model.showBuyerHomeWindow();
        else if(actionCommand.equals("add_product_final"))
        {
            Product product = new Product();
            product.setCategory(ProductUtility.parseCategory(view.getAddProductCategoryContents()));
            product.setPrice(Double.parseDouble(view.getAddProductPriceFieldContents()));
            product.setName(view.getAddProductNameFieldContents());
            product.setStock(Integer.parseInt(view.getAddProductStockFieldContents()));
            product.setDescription(view.getAddProductDescriptionFieldContents());

            model.addProduct(product, (Seller)view.getActiveUser());
        }
    }

    private void parseCategoryListSelectionEvent(int index)
    {
        if     (index == 0)
            model.setProductListDisplay("Home Goods", view);
        else if(index == 1)
            model.setProductListDisplay("Electronics", view);
        else if(index == 2)
            model.setProductListDisplay("Furniture", view);
        else if(index == 3)
            model.setProductListDisplay("Books", view);
        else if(index == 4)
            model.setProductListDisplay("Digital Goods", view);
    }
    
    private void parseProductListSelectionEvent(int index, JList list)
    {
        if (view.getActiveUser().getType() == 0)
        {
            model.showBuyerViewProductWindow(ProductUtility.getProduct((String)list.getSelectedValue()), view.getViewProductPanel());
        }
        else
        {
            model.showSellerViewProductWindow(ProductUtility.getProduct((String)list.getSelectedValue()), view.getViewProductPanel());
        }
    }

    AuctionModel model;
    AuctionView  view;
}
