import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

/**
 * Write a description of class ViewProductPanel here.
 * 
 * @author Christian Aghyarian
 * @version 30.04.2016
 */
public class ViewProductPanel extends JPanel
{
    public ViewProductPanel(AuctionController controller)
    {
        _productDescriptionBox = new JTextArea  ();
        _productNameLabel      = new JLabel     ();
        _priceLabel            = new JLabel     ();
        _stockLabel            = new JLabel     ();
        _skuLabel              = new JLabel     ();
        _descriptionScrollPane = new JScrollPane(_productDescriptionBox);
        _sellerReturnButton    = new JButton    ("Return");
        _buyerReturnButton     = new JButton    ("Return");
        _addToCartButton       = new JButton    ("Add to Cart");

        this.setLayout(null);

        this.add(_descriptionScrollPane);
        this.add(_productNameLabel     );
        this.add(_priceLabel           );
        this.add(_stockLabel           );
        this.add(_skuLabel             );
        this.add(_sellerReturnButton   );
        this.add(_buyerReturnButton    );
        this.add(_addToCartButton      );

        AbsolutePositionUtility.put(this, _descriptionScrollPane, 50 , 100, 400, 300);
        AbsolutePositionUtility.put(this, _productNameLabel     , 50 , 50 , 600, 30 );
        AbsolutePositionUtility.put(this, _priceLabel           , 500, 100, 300, 30 );
        AbsolutePositionUtility.put(this, _stockLabel           , 500, 130, 300, 30 );
        AbsolutePositionUtility.put(this, _skuLabel             , 500, 160, 300, 30 );
        AbsolutePositionUtility.put(this, _sellerReturnButton   , 500, 250, 150, 50 );
        AbsolutePositionUtility.put(this, _buyerReturnButton    , 500, 250, 150, 50 );
        AbsolutePositionUtility.put(this, _addToCartButton      , 500, 300, 150, 50 );

        _productDescriptionBox.setLineWrap(true);
        _productDescriptionBox.setEditable(false);
        
        PanelUtility.addEvent(controller, _sellerReturnButton, "seller_return_home");
        PanelUtility.addEvent(controller, _buyerReturnButton,  "buyer_return_home" );
        PanelUtility.addEvent(controller, _addToCartButton,    "add_to_cart");
    }

    public void displayProduct(Product product)
    { 
        DecimalFormat df = new DecimalFormat("0.00");
        String formattedPrice = df.format(product.getPrice());
        _priceLabel.setText("Price: $" + formattedPrice);
        _productNameLabel.setText("<html><span style='font-size:16px'>"+product.getName()+"</span></html>");
        _stockLabel.setText("Stock: " + String.valueOf(product.getStock()));
        _skuLabel.setText("SKU:   " + String.valueOf(product.getSkuNumber()));
        _productDescriptionBox.setText(product.getDescription());
    }

    public void setUserType(int type)
    {
        if(type == 0){
            _buyerReturnButton.setVisible(true);
            _sellerReturnButton.setVisible(false);
            _addToCartButton.setVisible(true);
        }
        else
        {
            _buyerReturnButton.setVisible(false);
            _sellerReturnButton.setVisible(true);
            _addToCartButton.setVisible(false);
        }
    }

    private JLabel      _productNameLabel;
    private JLabel      _priceLabel;
    private JLabel      _stockLabel;
    private JLabel      _skuLabel;
    private JTextArea   _productDescriptionBox;
    private JScrollPane _descriptionScrollPane;
    private JButton     _sellerReturnButton;
    private JButton     _buyerReturnButton;
    private JButton     _addToCartButton;
}
