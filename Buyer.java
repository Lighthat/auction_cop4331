import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Locale;
import java.util.*;

/**
 *
 * @author ryanspc
 */
public final class Buyer extends AbstractUser{

    public static final String TABLE_USERS         = "Users";
    public static final String COLUMN_ID           = "ID";
    public static final String COLUMN_SHOPPINGCART = "List";
    public static final String TABLE_PRODUCTS      = "Products";
    public static final String COLUMN_SKU          = "SKU";

    //private String PATH_TO_DB = "/home/caghyarian/Documents/dev/JAVA/Auction";
    private String PATH_TO_DB = "D:\\dev\\auction\\projPassworddatabase.sqlite";

    private int          _id;
    private String       _username;
    private String       _email;
    private Session      _session;
    private ShoppingCart _shoppingCart;

    public Buyer(int id, String username, String email){  
        _id           = id;
        _username     = username;
        _email        = email;
        _session      = new Session();
        _shoppingCart = populateShoppingCart(id);
    }

    //get methods
    public int          getID           () {return _id;           }

    public String       getUsername     () {return _username;     }

    public Session      getSession      () {return _session;      }

    public String       getEmail        () {return _email;        }

    public ShoppingCart getShoppingCart () {return _shoppingCart; }
    
    public void setShoppingCart(ShoppingCart cart) { _shoppingCart = cart; }

    @Override
    public int getType() { return 0; }

    private ShoppingCart populateShoppingCart(int id)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement statement = connection.createStatement();
            ResultSet resultSet = null;
            try{
                //Pull the shopping cart from the database
                String query = ("SELECT " + COLUMN_SHOPPINGCART + " FROM " + TABLE_USERS + " WHERE " + COLUMN_ID + " = " + id);
                resultSet = statement.executeQuery(query);
                if(resultSet.next())
                {
                    ArrayList<Product> productList   = new ArrayList<>();
                    String             productIDList = resultSet.getString("List");
                    if(productIDList != null){
                        List<String>       parsedIDList  = Arrays.asList(productIDList.split(","));
                        for(int i = 0; i < parsedIDList.size(); ++i)
                        {
                            query = "SELECT * FROM "+TABLE_PRODUCTS+" WHERE "+COLUMN_SKU+" = "+Integer.parseInt(parsedIDList.get(i));
                            resultSet = statement.executeQuery(query);
                            if(resultSet.next())
                            {
                                Product.Category category    = ProductUtility.parseCategory(resultSet.getString("Category"));
                                int              skuNumber   = resultSet.getInt   ("SKU");
                                double           price       = resultSet.getDouble("Price");
                                String           name        = resultSet.getString("name");
                                int              stock       = resultSet.getInt   ("stock");
                                String           description = resultSet.getString("descr");
                                productList.add(new Product(category, skuNumber, price, name, stock, description));
                            }
                        }
                        return new ShoppingCart(productList);
                    }
                    return new ShoppingCart();
                }            
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
            finally
            {
                if(resultSet != null)
                {
                    resultSet.close();
                }
                statement.close();
                connection.close();
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

        return null;
    }
}