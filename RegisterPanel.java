
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Write a description of class RegisterPanel here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RegisterPanel extends JPanel
{
    public RegisterPanel(AuctionController controller)
    {
        _registerBuyerButton  = new JButton("Buyer" );
        _registerSellerButton = new JButton("Seller");
        _usernameField      = new JTextField    (           );
        _passwordField      = new JPasswordField(           );
        _emailField         = new JTextField    (           );
        _usernameFieldLabel = new JLabel        ("Username:");
        _passwordFieldLabel = new JLabel        ("Password:");
        _emailFieldLabel    = new JLabel        ("E-mail:"  );

        this.setLayout(null);

        this.add(_registerBuyerButton    );
        this.add(_registerSellerButton);
        this.add(_usernameField     );
        this.add(_passwordField     );
        this.add(_emailField        );
        this.add(_usernameFieldLabel);
        this.add(_passwordFieldLabel);
        this.add(_emailFieldLabel   );

        AbsolutePositionUtility.put(this, _registerBuyerButton , 325, 300, 75 , 50);
        AbsolutePositionUtility.put(this, _registerSellerButton, 400, 300, 75 , 50);
        AbsolutePositionUtility.put(this, _usernameField       , 400, 150, 150, 30);
        AbsolutePositionUtility.put(this, _passwordField       , 400, 200, 150, 30);
        AbsolutePositionUtility.put(this, _emailField          , 400, 250, 150, 30);
        AbsolutePositionUtility.put(this, _usernameFieldLabel  , 250, 150, 150, 30);
        AbsolutePositionUtility.put(this, _passwordFieldLabel  , 250, 200, 150, 30);
        AbsolutePositionUtility.put(this, _emailFieldLabel     , 250, 250, 150, 30);
        
        
        PanelUtility.addEvent(controller, _registerBuyerButton, "register_buyer");
        PanelUtility.addEvent(controller, _registerSellerButton,"register_seller");
    }
    
    public String getUsernameFieldContents() { return _usernameField.getText(); }
    
    public String getPasswordFieldContents() { return new String(_passwordField.getPassword()); }
    
    public String getEmailFieldContents   () { return _emailField.getText();    }
    
    
    private JButton        _registerBuyerButton;
    private JButton        _registerSellerButton;
    private JTextField     _usernameField;
    private JPasswordField _passwordField;
    private JTextField     _emailField;
    private JLabel         _usernameFieldLabel;
    private JLabel         _passwordFieldLabel;
    private JLabel         _emailFieldLabel;
}
