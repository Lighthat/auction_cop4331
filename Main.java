
/**
 * Serves as the primary entry point for the program.
 * 
 * @author Christian Aghyarian
 * @version 23.04.16
 */
public class Main
{

    public static void main(String args[])
    {
        AuctionController controller = new AuctionController(          );
        AuctionView       view       = new AuctionView      (controller);
        controller.addView (view           );
        controller.addModel(view.getModel());
    }
}
