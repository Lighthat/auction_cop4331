import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * This is a static class that contains a series of useful utility methods when designing
 * a Swing GUI using aboslute position coordinates, as opposed to using a layout manager.
 * 
 * Static classes don't formally exist in Java, but this is functionally a static class
 * as it is declared as final, and has a private constructor. It should never be
 * instantiated, as described by the Utility design pattern.
 *
 * @author Christian Aghyarian
 * @version 27.04.16
 */
public final class AbsolutePositionUtility
{
    /**
     * Places a JComponent in the provided JJFrame.
     * 
     * @param frame the JFrame you wish to add the component to
     * @param component the JComponent you wish to add
     * @param x the x coordinate of where you want the component to go
     * @param y the y coordinate of where you want the component to go
     */
    public static void put(JFrame frame, JComponent component, int x, int y)
    {
        Insets insets = frame.getInsets();
        Dimension size = component.getPreferredSize();
        component.setBounds(x + insets.left, y + insets.top, size.width, size.height);
    }
    
    /**
     * Puts a JComponent in the provided JPanel.
     * 
     * @param panel the JPanel you wish to add the component to
     * @param component the JComponent you wish to add
     * @param x the x coordinate of where you want the component to go
     * @param y the y coordinate of where you want the component to go
     */
    public static void put(JPanel panel, JComponent component, int x, int y)
    {
        Insets insets = panel.getInsets();
        Dimension size = component.getPreferredSize();
        component.setBounds(x + insets.left, y + insets.top, size.width, size.height);
    }
    
    /**
     * Puts JComponent in the provided JFrame. 
     * This function supports custom width and height.
     * 
     * @param frame the JFrame you wish to add the component to
     * @param component the JComponent you wish to add
     * @param x the x coordinate of where you want the component to go
     * @param y the y coordinate of where you want the component to go
     * @param width the width of the component
     * @param height the height of the component
     */
    public static void put(JFrame frame, JComponent component, int x, int y, int width, int height)
    {
        Insets insets = frame.getInsets();
        component.setBounds(x + insets.left, y + insets.top, width, height);
    }
    
    /**
     * Puts a JComponent in the provided JPanel. 
     * This function supports custom width and height.
     * 
     * @param panel the JPanel you wish to add the component to
     * @param component the JComponent you wish to add
     * @param x the x coordinate of where you want the component to go
     * @param y the y coordinate of where you want the component to go
     * @param width the width of the component
     * @param height the height of the component
     */
    public static void put(JPanel panel, JComponent component, int x, int y, int width, int height)
    {
        Insets insets = panel.getInsets();
        component.setBounds(x + insets.left, y + insets.top, width, height);
    }
    
    private AbsolutePositionUtility(){ }
}
