import java.util.ArrayList;
import java.util.Iterator;

/**
 * Serves as a container for all of the active Products a Seller offers.
 * 
 * @author Christian Aghyarian
 * @version 19.04.16
 */
public class Inventory implements Iterable
{
    /**
     * Creates an Inventory object with an empty product list.
     * 
     * @param owner the ID of the Seller this Inventory belongs to
     */
    public Inventory()
    {
        _productList = new ArrayList<>();
    }
    
    /**
     * Creates an Inventory object by importing an existing list of products.
     * 
     * @param owner the ID of the Seller this Inventory belongs to
     * @param list  the product list being imported
     */
    public Inventory(ArrayList<Product> list)
    {
        _productList = new ArrayList<>(list);
    }
    
    /**
     * @return an Iterator for this Inventory.
     */
    public Iterator iterator()
    {        
        return new InventoryIterator();
    }
    
    /**
     * Adds a Product to this Inventory.
     * 
     * @param product the Product you wish to add
     */
    public void addProduct(Product product)
    {
        _productList.add(product);
    }
    
    /**
     * Tries to remove a Product from this Inventory.
     * 
     * @param skuNumber the SKU number of the Product you wish to remove
     * @return True if the product was in this Inventory, false otherwise.
     */
    public boolean removeProduct(int skuNumber)
    {
        //Search for a Product with the given SKU number
        for(int i = 0; i < _productList.size(); ++i)
        {
            if(_productList.get(i).getSkuNumber() == skuNumber)
            {
                _productList.remove(i);
                return true;
            }
        }
        
        return false;
    }
    
    public int size()
    {
        return _productList.size();
    }
    
    
    /**
     * Allows a user to iterate over the Inventory.
     * 
     * @author Christian Aghyarian
     * @version 19.04.16
     */
    private class InventoryIterator implements Iterator
    {
        @Override
        public boolean hasNext()
        {
            if(index < _productList.size())
                return true;
            else
                return false;
        }
        
        @Override
        public Product next()
        {
            if(this.hasNext())
                return _productList.get(index++);
            else
                return null;
        }
        
        @Override
        public void remove()
        {
            
        }
        
        int index;
    }
    
    private ArrayList<Product> _productList;
}
