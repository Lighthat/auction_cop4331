
/**
 * Serves as a superclass for the types of users the Auction program will encounter.
 * 
 * @author ryanspc
 * @version 26.04.16
 */

import java.sql.*;

public abstract class AbstractUser {

    /** The ID of the current user       */
    private int     _id; 
    /** The email of the current user    */
    private String  _email;
    /** The username of the current user */
    private String  _username;
    /** A handler to make sure a user can
     *  only be logged in once           */
    private Session _session;

    //private static String PATH_TO_DB = "/home/caghyarian/Documents/dev/JAVA/Auction";
    private static String PATH_TO_DB = "D:\\dev\\auction\\projPassworddatabase.sqlite";

    public static final String COLUMN_Username = "UserName";
    public static final String COLUMN_PW = "Password";
    public static final String TABLE_USERS = "Users";

    //get methods
    public int getID         () {return _id; }

    public String getUsername() {return _username; }

    public Session getSession() {return _session;}

    public String getEmail   () {return _email;}

    //set methods
    public void setID      (int ID)         { _id       = ID;}

    public void setUsername(String username){ _username = username; }

    public void setSession (Session S)      { _session  = S;}

    public void setEmail   (String email)   { _email    = email;}

    /**
     * Takes a username and a hashed password and runs a database query to verify that the information is valid
     * As a note, throwing custom exceptions would be better than returning negative numbers for errors,
     * but this could be revisited later during a short refactor if we have time.
     * 
     * This method is static since you will need to actually validate a user before you take the steps
     * necessary to create one. 
     * 
     * @param username The username of the user who is trying to login
     * @param password The hashed password that the user entered
     * @return The ID of the user who logged in, otherwise, a negative number depending on the error
     */
    public static int login (String username, String password)
    {
        //Verify the file exists
        try{
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement st=con.createStatement();
            ResultSet rs = null;
            try{
                //set up connection with the database I made in sqlite        

                //Check if login information is valid
                String query = ("Select * FROM Users WHERE " + COLUMN_Username + " = \"" + username + "\" AND " + COLUMN_PW  +  " = \"" + password +"\"");
                rs=st.executeQuery(query);

                //If the query returned a result, the login was successful
                if(rs.next()){
                    String sessionFromDatabase = rs.getString("current Session");
                    if(sessionFromDatabase == null)
                    {           
                        //the credentials were valid, and they're not already logged in
                        return rs.getInt("ID");
                    }    
                    //credentials are valid, but user is already logged in
                    else if(rs.next())
                    {
                        return -2;
                    }
                    //close the connection with the database
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(rs != null){
                    rs.close();
                }
                st.close();
                con.close();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        //no match with the password or username    
        return -1;
    }

    public static boolean register(String username, String password, String email, int type)
    {
        try{
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + PATH_TO_DB);
            Statement st=con.createStatement();
            ResultSet rs = null;
            try{  
                //Check if account already exists
                String query = ("Select * FROM Users WHERE " + COLUMN_Username + " = \"" + username + "\"");
                rs=st.executeQuery(query);

                //If the query returned a result, registration failed because user already exists
                if(rs.next()){
                    System.out.println("test");
                    return false;
                }

                query = ("INSERT INTO Users (UserName, Password, email, Type, revenue) VALUES (\""+username+"\", \""+password+"\", \""+email+"\","+type+", 0.00)");
                st.executeUpdate(query);
                System.out.println(query);
                System.out.println("return true");
                return true;

            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
                return false;
            }
            finally
            {
                rs.close();
                st.close();
                con.close();
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public abstract int getType();

    /**
     * Cleans up all necessary information associated with a user when they log out (or if their Session expires)
     * 
     * @param ID the ID of the user who should be logged out
     */
    public void logout(int ID)
    {
        _session.destroy();
    }
}