import javax.swing.*;

/**
 * Serves as a container for Users to log in. Intended to be instantiated and then
 * placed in the main JFrame in the View, then hidden and shown as needed.
 * 
 * @author Christian Aghyarian
 * @version 27.04.16
 */
public class LoginPanel extends JPanel
{
    /**
     * Constructs the panel
     */
    public LoginPanel(AuctionController controller)
    {        
        _loginButton        = new JButton       ("Login"    );
        _exitButton         = new JButton       ("Exit"     );
        _registerButton     = new JButton       ("Register" );
        _usernameField      = new JTextField    (           );
        _passwordField      = new JPasswordField(           );
        _usernameFieldLabel = new JLabel        ("Username:");
        _passwordFieldLabel = new JLabel        ("Password:");

        this.setLayout(null);

        this.add(_loginButton       );
        this.add(_exitButton        );
        this.add(_usernameField     );
        this.add(_passwordField     );
        this.add(_usernameFieldLabel);
        this.add(_passwordFieldLabel);
        this.add(_registerButton    );

        AbsolutePositionUtility.put(this, _loginButton,        50,  110, 150, 20);
        AbsolutePositionUtility.put(this, _exitButton,         200, 110, 150, 20);
        AbsolutePositionUtility.put(this, _registerButton,     125, 130, 150, 20);
        AbsolutePositionUtility.put(this, _usernameField,      170, 10,  200, 20);
        AbsolutePositionUtility.put(this, _passwordField,      170, 60,  200, 20);
        AbsolutePositionUtility.put(this, _passwordFieldLabel, 30,  60,  200, 20);
        AbsolutePositionUtility.put(this, _usernameFieldLabel, 30,  10,  200, 20);
        
        
        PanelUtility.addEvent(controller, _loginButton,    "login");
        PanelUtility.addEvent(controller, _exitButton,     "exit" );
        PanelUtility.addEvent(controller, _registerButton, "show_register_window");

    }
    
    public String getUsernameFieldContents() { return _usernameField.getText(); }
    
    public String getPasswordFieldContents() { return new String(_passwordField.getPassword()); }
    
    private JButton        _loginButton;
    private JButton        _exitButton;
    private JButton        _registerButton;
    private JTextField     _usernameField;
    private JPasswordField _passwordField;
    private JLabel         _usernameFieldLabel;
    private JLabel         _passwordFieldLabel;
}
